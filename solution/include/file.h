#ifndef FILE_H
#define FILE_H

#include "error.h"
#include <stdio.h>

struct status get_file_for_read(char const* file_name, FILE** file);
struct status get_file_for_write(char const* file_name, FILE** file);

struct status close_file(FILE* file);

#endif
