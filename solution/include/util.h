#ifndef UTIL_H
#define UTIL_H

#include "bmp.h"
#include "error.h"
#include "file.h"

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct maybe_int32_t {
    bool valid;
    int32_t value;
};

struct maybe_int32_t convert_angle(char* angle);
struct status check_args(const int argc, char** argv);

#endif
