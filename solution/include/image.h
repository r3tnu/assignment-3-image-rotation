#ifndef IMAGE_H
#define IMAGE_H

#include "error.h"

#include <inttypes.h>
#include <stdio.h>

struct pixel {
    uint8_t b, g, r;
};

struct image {
    int64_t width, height;
    struct pixel *data;
};

size_t get_size_bytes(const struct image img);
struct status image_copy_and_rotate(const struct image* img, int32_t angle, struct image* new_img);
void free_image(struct image img);

#endif
