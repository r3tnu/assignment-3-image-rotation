#ifndef ERROR_H
#define ERROR_H

#include <stdio.h>


enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_ERROR
};

enum write_status {
    WRITE_OK = 0,
    WRITE_HEADER_ERROR,
    WRITE_DATA_ERROR,
    WRITE_ERROR
};

enum open_status {
    OPEN_OK = 0,
    OPEN_FILENAME_ERROR,
    OPEN_ERROR
};

enum close_status {
    CLOSE_OK = 0,
    CLOSE_ERROR
};

enum rotate_status {
    ROTATE_OK = 0,
    ROTATE_ANGLE_ERROR,
    ROTATE_DATA_ERROR,
    ROTATE_ERROR
};

enum arguments_status {
    ARGUMENTS_OK = 0,
    ARGUMENTS_TOO_MANY,
    ARGUMENTS_NOT_ENOUGH,
    ARGUMENTS_INVALID_ANGLE,
    ARGUMENTS_ERROR
};

struct status {
    enum status_type { READ_STATUS = 0, WRITE_STATUS, OPEN_STATUS, CLOSE_STATUS, ROTATE_STATUS, ARGUMENTS_STATUS } type;
    union {
        enum read_status as_read;

        enum write_status as_write;

        enum open_status as_open;

        enum close_status as_close;

        enum rotate_status as_rotate;

        enum arguments_status as_arguments;
    };
};

void printn(char* message);
void print_status(struct status status);
int get_status(struct status status);

#endif
