
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/home/r3tnu/University/Programming-Languages-3-Semester/Labs/assignment-3-image-rotation/solution/src/bmp.c" "solution/CMakeFiles/image-transformer.dir/src/bmp.c.o" "gcc" "solution/CMakeFiles/image-transformer.dir/src/bmp.c.o.d"
  "/home/r3tnu/University/Programming-Languages-3-Semester/Labs/assignment-3-image-rotation/solution/src/error.c" "solution/CMakeFiles/image-transformer.dir/src/error.c.o" "gcc" "solution/CMakeFiles/image-transformer.dir/src/error.c.o.d"
  "/home/r3tnu/University/Programming-Languages-3-Semester/Labs/assignment-3-image-rotation/solution/src/file.c" "solution/CMakeFiles/image-transformer.dir/src/file.c.o" "gcc" "solution/CMakeFiles/image-transformer.dir/src/file.c.o.d"
  "/home/r3tnu/University/Programming-Languages-3-Semester/Labs/assignment-3-image-rotation/solution/src/image.c" "solution/CMakeFiles/image-transformer.dir/src/image.c.o" "gcc" "solution/CMakeFiles/image-transformer.dir/src/image.c.o.d"
  "/home/r3tnu/University/Programming-Languages-3-Semester/Labs/assignment-3-image-rotation/solution/src/main.c" "solution/CMakeFiles/image-transformer.dir/src/main.c.o" "gcc" "solution/CMakeFiles/image-transformer.dir/src/main.c.o.d"
  "/home/r3tnu/University/Programming-Languages-3-Semester/Labs/assignment-3-image-rotation/solution/src/util.c" "solution/CMakeFiles/image-transformer.dir/src/util.c.o" "gcc" "solution/CMakeFiles/image-transformer.dir/src/util.c.o.d"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
