#include "image.h"
#include "error.h"

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

size_t get_size_bytes(const struct image img) {
    return img.width * img.height * 3;
}

static struct image rotate270(const struct image* img) {
    struct image new_img = {.width = img->height, .height = img->width, .data = malloc(sizeof(struct pixel) * img->width * img->height)};
    if (new_img.data == NULL) return (struct image) {.width = 0};
    struct pixel* data_pointer = new_img.data;
    for (int64_t i = 0; i < img->width; i++) {
        for (int64_t j = img->width * (img->height - 1); j >= 0; j = j - img->width) {
            *data_pointer = img->data[i + j];
            data_pointer++;
        }
    }
    return new_img;
}

static struct image rotate90(const struct image* img) {
    struct image new_img = {.width = img->height, .height = img->width, .data = malloc(sizeof(struct pixel) * img->width * img->height)};
    if (new_img.data == NULL) return (struct image) {.width = 0};
    struct pixel* data_pointer = new_img.data;
    for (int64_t i = img->width - 1; i >= 0; i--) {
        for (int64_t j = 0; j <= img->width * (img->height - 1); j = j + img->width) {
            *data_pointer = img->data[i + j];
            data_pointer++;
        }
    }
    return new_img;
}

static struct image rotate180(const struct image* img) {
    struct image new_img = {.width = img->width, .height = img->height, .data = malloc(sizeof(struct pixel) * img->width * img->height)};
    if (new_img.data == NULL) return (struct image) {.width = 0};
    struct pixel* data_pointer = new_img.data;
    for (int64_t i = img->width * (img->height - 1); i >= 0; i = i - img->width) {
        for (int64_t j = img->width - 1; j >= 0; j--) {
            *data_pointer = img->data[i + j];
            data_pointer++;
        }
    }
    return new_img;
}

static struct image rotate0(const struct image* img) {
    struct image new_img = {.width = img->width, .height = img->height, .data = malloc(sizeof(struct pixel) * img->width * img->height)};
    if (new_img.data == NULL) return (struct image) {.width = 0};
    struct pixel* data_pointer = new_img.data;
    for (int64_t i = 0; i < img->width * img->height; i++ ) {
        data_pointer[i] = img->data[i];
    }
    return new_img;
}


struct status image_copy_and_rotate(const struct image* img, int32_t angle, struct image* new_img) {
    switch (angle) {
        case 0:
        case 360: *new_img = rotate0(img); break;
        case 90:
        case -270: *new_img = rotate90(img); break;
        case 180:
        case -180: *new_img = rotate180(img); break;
        case 270:
        case -90: *new_img = rotate270(img); break;
        default: return (struct status) {ROTATE_STATUS, .as_rotate = ROTATE_ANGLE_ERROR};
   }

   if (new_img->width == 0) {
    return (struct status) {ROTATE_STATUS, .as_rotate = ROTATE_ERROR};
   }

    return (struct status) {ROTATE_STATUS, .as_rotate = ROTATE_OK};
}

void free_image(struct image img) {
    free(img.data);
}

