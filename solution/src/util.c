#include "util.h"
#include "bmp.h"
#include "error.h"
#include "file.h"

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct maybe_int32_t convert_angle(char* angle) {
    struct maybe_int32_t ang = { .value = atoi(angle), .valid = true };
    if (ang.value != 0) {
        return ang;
    }
    if (*angle == '0') {
        return ang;
    }
    ang.valid = false;
    return ang;
}

struct status check_args(const int argc, char** argv) {
    if (argc > 4) {
        return (struct status) {ARGUMENTS_STATUS, .as_arguments = ARGUMENTS_TOO_MANY};
    } else if (argc < 4) {
        return (struct status) {ARGUMENTS_STATUS, .as_arguments = ARGUMENTS_NOT_ENOUGH};
    }
    struct maybe_int32_t ang = convert_angle(argv[3]);
    if (!ang.valid) {
        return (struct status) {ARGUMENTS_STATUS, .as_arguments = ARGUMENTS_INVALID_ANGLE};
    }
    return (struct status) {ARGUMENTS_STATUS, .as_arguments = ARGUMENTS_OK};
}
