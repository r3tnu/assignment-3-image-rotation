#include "image.h"
#include "bmp.h"
#include "error.h"
#include "file.h"
#include "util.h"

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main( int argc, char** argv ) {
    char* filename;
    char* new_filename;
    int32_t angle = 0;
    FILE* file = NULL;
    struct image img = { .width = 0, .height = 0, .data = NULL };
    struct image new_img = img;
    struct status status;

    status = check_args(argc, argv);
    print_status(status);
    if (get_status(status)) {
        free_image(img);
        free_image(new_img);
        return get_status(status);
    }
    filename = argv[1];
    new_filename = argv[2];
    angle = convert_angle(argv[3]).value;

    status = get_file_for_read(filename, &file);
    print_status(status);
    if (get_status(status)) {
        free_image(img);
        free_image(new_img);
        return get_status(status);
    }

    status = from_bmp(file, &img);
    print_status(status);
    if (get_status(status)) {
        free_image(img);
        free_image(new_img);
        return get_status(status);
    }

    status = close_file(file);
    print_status(status);
    if (get_status(status)) {
        free_image(img);
        free_image(new_img);
        return get_status(status);
    }

    status = image_copy_and_rotate(&img, angle, &new_img);
    print_status(status);
    if (get_status(status)) {
        free_image(img);
        free_image(new_img);
        return get_status(status);
    }

    status = get_file_for_write(new_filename, &file);
    print_status(status);
    if (get_status(status)) {
        free_image(img);
        free_image(new_img);
        return get_status(status);
    }

    status = to_bmp(file, &new_img);
    print_status(status);
    if (get_status(status)) {
        free_image(img);
        free_image(new_img);
        return get_status(status);
    }

    status = close_file(file);
    print_status(status);
    if (get_status(status)) {
        free_image(img);
        free_image(new_img);
        return get_status(status);
    }


    free_image(img);
    free_image(new_img);
    return 0;
}
