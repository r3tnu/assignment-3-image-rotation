#include "error.h"

#include <stdio.h>

void printn(char* message) {
    fprintf(stderr, "%s\n", message);
}

void print_status(struct status status) {
    switch (status.type) {
        case READ_STATUS: {
            switch (status.as_read) {
                case READ_OK: printn("Status: READ_OK"); break;
                case READ_INVALID_SIGNATURE: printn("Error: READ_INVALID_SIGNATURE"); break;
                case READ_INVALID_BITS: printn("Error: READ_INVALID_BITS"); break;
                case READ_INVALID_HEADER: printn("Error: READ_INVALID_HEADER"); break;
                default: printn("Error: READ_ERROR"); break;
            } break;
        }
        case WRITE_STATUS: {
            switch (status.as_write) {
                case WRITE_OK: printn("Status: WRITE_OK"); break;
                case WRITE_HEADER_ERROR: printn("Error: WRITE_HEADER_ERROR"); break;
                case WRITE_DATA_ERROR: printn("Error: WRITE_DATA_ERROR"); break;
                default: printn("Error: WRITE_ERROR"); break;
            } break;
        }
        case OPEN_STATUS: {
            switch (status.as_open) {
                case OPEN_OK: printn("Status: OPEN_OK"); break;
                case OPEN_FILENAME_ERROR: printn("Error: OPEN_FILENAME_ERROR");
                default: printn("Error: OPEN_ERROR"); break;
            } break;
        }
        case CLOSE_STATUS: {
            switch (status.as_close) {
                case CLOSE_OK: printn("Status: CLOSE_OK"); break;
                default: printn("Error: CLOSE_ERROR"); break;
            } break;
        }
        case ROTATE_STATUS: {
            switch (status.as_rotate) {
                case ROTATE_OK: printn("Status: ROTATE_OK"); break;
                case ROTATE_ANGLE_ERROR: printn("Error: ROTATE_ANGLE_ERROR"); break;
                case ROTATE_DATA_ERROR: printn("Error: ROTATE_DATA_ERROR"); break;
                default: printn("Error: ROTATE_ERROR"); break;
            } break;
        }
        case ARGUMENTS_STATUS: {
            switch (status.as_arguments) {
                case ARGUMENTS_OK: printn("Status: ARGUMENTS_OK"); break;
                case ARGUMENTS_TOO_MANY: printn("Error: ARGUMENTS_TOO_MANY_ERROR"); break;
                case ARGUMENTS_NOT_ENOUGH: printn("Error: ARGUMENTS_NOT_ENOUGH_ERROR"); break;
                case ARGUMENTS_INVALID_ANGLE: printn("Error: WRONG_ANGLE"); break;
                default: printn("Error: ARGUMENTS_ERROR"); break;
            } break;
        }
    }
}

int get_status(struct status status) {
    switch (status.type) {
        case READ_STATUS: return status.as_read;
        case WRITE_STATUS: return status.as_write;
        case OPEN_STATUS: return status.as_open;
        case CLOSE_STATUS: return status.as_close;
        case ROTATE_STATUS: return status.as_rotate;
        case ARGUMENTS_STATUS: return status.as_arguments;
        default: return 1;
    }
}
