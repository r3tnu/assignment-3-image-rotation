#include "file.h"
#include "error.h"

#include <stdio.h>

struct status get_file_for_read(char const* file_name, FILE** file) {
    *file = fopen(file_name, "rb");
    if (*file) {
        return (struct status) { OPEN_STATUS, .as_open = OPEN_OK};
    }
    return (struct status) {OPEN_STATUS, .as_open = OPEN_ERROR};
}

struct status get_file_for_write(char const* file_name, FILE** file) {
    *file = fopen(file_name, "wb");
    if (*file) {
        return (struct status) { OPEN_STATUS, .as_open = OPEN_OK};
    }
    return (struct status) {OPEN_STATUS, .as_open = OPEN_ERROR};
}

struct status close_file(FILE* file) {
    if (!fclose(file)) {
        return (struct status) { CLOSE_STATUS, .as_close = CLOSE_OK };
    }
    return (struct status) { CLOSE_STATUS, .as_close = CLOSE_ERROR };
}
