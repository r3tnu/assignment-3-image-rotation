#include "bmp.h"
#include "error.h"
#include "image.h"

#include <stdio.h>
#include <stdlib.h>

static const int BYTES_PER_PIXEL = 3;


struct bmp_header generate_bmp_header(uint64_t height, uint64_t width, int64_t stride) {
        struct bmp_header header;

    header.bfType = 0x4d42;
    header.bfileSize = sizeof(struct bmp_header) + (stride * height);
    header.bfReserved = 0;
    header.bOffBits = sizeof(struct bmp_header);


    header.biSize = sizeof(struct bmp_header) - 14;
    header.biWidth = width;
    header.biHeight = height;
    header.biPlanes = 1;
    header.biBitCount = BYTES_PER_PIXEL * 8;
    header.biCompression = 0;
    header.biSizeImage = 0;
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;

    return header;
}

struct status from_bmp(FILE* in, struct image* img) {
    struct bmp_header header;
    fread(&header, sizeof(struct bmp_header), 1, in);
    if (ferror(in) || feof(in)) {
        return (struct status) { READ_STATUS, .as_read = READ_INVALID_HEADER};
    }

    if (header.bfType != 0x4D42) {
        return (struct status) { READ_STATUS, .as_read = READ_INVALID_SIGNATURE};
    }

    if (header.biBitCount != 24) {
        return (struct status) {READ_STATUS, .as_read = READ_INVALID_HEADER};
    }

    img->width = header.biWidth;
    img->height = header.biHeight;
    size_t size = header.biWidth * header.biHeight;

    int64_t widthInBytes = img->width * BYTES_PER_PIXEL;
    int64_t paddingSize = (4 - (widthInBytes) % 4) % 4;

    struct pixel* data = malloc(size * sizeof(struct pixel));
    if (!data) {
        return (struct status) { READ_STATUS, .as_read = READ_ERROR};
    }
    for (size_t i = 0; i < img->height; i++) {
        fread(&data[img->width * i], sizeof(struct pixel), img->width, in);
        fseek(in, paddingSize, SEEK_CUR);
    }
    if (ferror(in) || feof(in) || !data) {
        return (struct status) { READ_STATUS, .as_read = READ_INVALID_BITS };
    }
    img->data = data;
    return (struct status) { READ_STATUS, .as_read = READ_OK };
}

struct status to_bmp(FILE* out, struct image const* img) {
    int64_t widthInBytes = img->width * BYTES_PER_PIXEL;
    int64_t paddingSize = (4 - (widthInBytes) % 4) % 4;
    int64_t stride = widthInBytes + paddingSize;
    int padding[3] = {0, 0, 0};

    struct bmp_header header = generate_bmp_header(img->height, img->width, stride);

    fwrite(&header, sizeof(struct bmp_header), 1, out);
    if (ferror(out)) {
        return (struct status) { WRITE_STATUS, .as_write = WRITE_HEADER_ERROR};
    }
    for (size_t i = 0; i < img->height; i++) {
        fwrite(&img->data[img->width * i], sizeof(struct pixel), img->width, out);
        if (paddingSize > 0) {
            fwrite(padding, 1, paddingSize, out);
        }
    }
    if (ferror(out)) {
        return (struct status) {WRITE_STATUS, .as_write = WRITE_DATA_ERROR};
    }

    return (struct status) { WRITE_STATUS, .as_write = WRITE_OK };
}

