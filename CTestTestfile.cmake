# CMake generated Testfile for 
# Source directory: /home/r3tnu/University/Programming-Languages-3-Semester/Labs/assignment-3-image-rotation
# Build directory: /home/r3tnu/University/Programming-Languages-3-Semester/Labs/assignment-3-image-rotation
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("solution")
subdirs("tester")
