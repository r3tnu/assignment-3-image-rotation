
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/home/r3tnu/University/Programming-Languages-3-Semester/Labs/assignment-3-image-rotation/tester/src/bmp.c" "tester/CMakeFiles/image-matcher.dir/src/bmp.c.o" "gcc" "tester/CMakeFiles/image-matcher.dir/src/bmp.c.o.d"
  "/home/r3tnu/University/Programming-Languages-3-Semester/Labs/assignment-3-image-rotation/tester/src/file_cmp.c" "tester/CMakeFiles/image-matcher.dir/src/file_cmp.c.o" "gcc" "tester/CMakeFiles/image-matcher.dir/src/file_cmp.c.o.d"
  "/home/r3tnu/University/Programming-Languages-3-Semester/Labs/assignment-3-image-rotation/tester/src/main.c" "tester/CMakeFiles/image-matcher.dir/src/main.c.o" "gcc" "tester/CMakeFiles/image-matcher.dir/src/main.c.o.d"
  "/home/r3tnu/University/Programming-Languages-3-Semester/Labs/assignment-3-image-rotation/tester/src/util.c" "tester/CMakeFiles/image-matcher.dir/src/util.c.o" "gcc" "tester/CMakeFiles/image-matcher.dir/src/util.c.o.d"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
